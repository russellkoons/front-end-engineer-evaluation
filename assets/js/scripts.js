// I went ahead and made it so you can search for places by name as opposed to just coordinates

$(document).ready(function() {
    // Makes a starting call to set everything for local Atlanta weather.
    getLocation('33.7490,-84.3880');
    document.getElementById('search-btn').addEventListener('click', function(event) {
        var input = $('#weather-input').val();
        getLocation(input);
    });
});

function getLocation(input) {
    // I used the OpenCage Geocoder API to find location based on input, then call the weather API

    var query = encodeURIComponent(input);
    var locationUrl = 'https://api.opencagedata.com/geocode/v1/json?q=' + query + '&key=c3b7dad01ec641f2b97d30f131bf7cab';
    var coords;
    var location;

    fetch(locationUrl)
        .then(function(res) {
            if (res.ok) {
                return res.json();
            } else {
                throw new Error(res.statusText);
            }
        })
        .then(function(resJson) {
            coords = resJson.results[0].geometry.lat + ',' + resJson.results[0].geometry.lng;
            location = resJson.results[0].components.city + ', ' + resJson.results[0].components.state_code;
            getWeatherData(coords, location);
        })
        .catch(function(e) {
            alert(e);
        });
}

function getWeatherData(input, location) {
    var secretKey = '66d45a946873b49bea48620a3dcf36a2'; // <-- Enter DarkSky key here

    // DarkSky seems to only take requests from certain URLs so I used this cors anywhere app to bypass the cors requirement

    var url = 'https://cors-anywhere.herokuapp.com/https://api.darksky.net/forecast/' + secretKey + '/' + input;

    // Gets weather info

    fetch(url)
        .then(function(res) {
            if (res.ok) {
                return res.json();
            } else {
                throw new Error(res.statusText);
            }
        })
        .then(function(resJson) {
            updateHTML(resJson, location);
        })
        .catch(function(e) {
            alert(e);
        });
}

function updateHTML(res, loc) {
    // Sets date, time and location in the top corner

    var date = $.date();
    var weekday = $.date('getDayName');
    var day = $.date('j');
    var month = $.date('getMonthName');
    var time = $.date('h:iA');

    $('#current-date').empty().append(weekday + ', ' + month + ' ' + day + ', 2019');
    $('#current-time').empty().append(time);

    $('#location').empty().append(loc);

    // Sets the days of the week in the 5 day forecast

    for (var i = 0; i < 4; i++) {
        date = $.date(date, 'addDays', 1);
        $('#next-' + i).empty().append($.date(date, 'getDayName', true));
    }

    // Converts the icons from the response into font-awesome icons and sets them in the 5 day forecast

    for (var i = 0; i < 5; i++) {
        var icon = res.daily.data[i].icon;

        if (icon === 'clear-day' || icon === 'clear-night') {
            icon = 'fa-sun';
        } else if (icon === 'partly-cloudy-day' || icon === 'partly-cloudy-night') {
            icon = 'fa-cloud-sun';
        } else if (icon === 'cloudy') {
            icon = 'fa-cloud';
        } else if (icon === 'rain' || icon === 'sleet') {
            icon = 'fa-cloud-showers-heavy';
        } else if (icon === 'windy') {
            icon = 'fa-wind';
        } else if (icon === 'snow') {
            icon = 'fa-snow';
        } else {
            icon = 'fa-smog';
        }

        $('#icon-' + i).removeClass('fa-sun fa-cloud-sun fa-cloud fa-cloud-showers-heavy fa-wind fa-snow fa-smog').addClass(icon);
    }

    // Sets the temperatures and weekly summary

    $('#current-temp').empty().append(Math.round(res.currently.temperature) + '&deg;');
    $('#current-weather').empty().append(res.currently.summary);
    $('#current-summary').empty().append(res.daily.summary);

    for (var i = 0; i < 5; i++) {
        $('#hi-' + (i + 1)).empty().append(Math.round(res.daily.data[i].temperatureHigh) + '&deg;');
        $('#lo-' + (i + 1)).empty().append(Math.round(res.daily.data[i].temperatureLow) + '&deg;');
    }
}
